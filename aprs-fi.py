#!/bin/python3

#to install pip, use: apt install python3-pip

import wget # for web file download.  install with: sudo pip3 install python3-wget
import sys # for file I/O
import os # for removing files
import re # for regex
import pymap3d as pm #for geodetic2aer.  install with: sudo python3 -m pip install pymap3d
import time #for sleep
import serial #for serial interaction with the M2 antenna controller

# Lat and Long of Ground Station (West Point, NY)
WPLat = 41.23 #North
WPLong = -73.57 #West

if (len(sys.argv) < 2):
  print("Call sign not provided.  Using W2KGY-11");
  callSign = "W2KGY-11"
else:
  print("Using callsign: " + sys.argv[1])
  callSign = sys.argv[1]

#///////
# Code to set up serial connection to M2 controller
azcom = '/dev/ttyUSB2'
elcom = '/dev/ttyUSB0'  # These need to be configured this way in the OS
baudrate = 9600
t = 200 # A max count - with a wait time of 2 minutes, should run for max of 6.6 hours
c = 0

# Establish serial connections
try:
  azser = serial.Serial(azcom, baudrate, timeout=1)
except Exception as e:
  print(e)
  print("Failed to open azimuth", azcom)
  azser = 0
try:
  elser = serial.Serial(elcom, baudrate, timeout=1)
except Exception as e:
  print(e)
  print("Failed to open elevation", elcom)
  elser = 0
#///////

if (os.path.exists(callSign)):
  os.remove(callSign) #ensure there are no old pages

print("Using following coordinates for West Point:")
print("Latitude: " + str(WPLat))
print("Longitude: " + str(WPLong))
print()

baseUrl = "https://aprs.fi/info/a/"
fullUrl = baseUrl + callSign

while(c < t):
  filename = wget.download(fullUrl)

  print("\nFile received: " + filename)
  print()

  file = open(filename, 'r')
  contents = file.read()

  # Get Lat/Long
  for line in contents.split("\n"):
    if "Location:" in line:
      string = line.strip()
      break

  string = string[15:90]

  expression = "[0-9][0-9]°[0-9][0-9]"
  p = re.compile(expression)
  results = p.findall(string)

  latsplit = results[0].split('°')
  longsplit = results[1].split('°')
  lat = int(latsplit[0]) + (int(latsplit[1])/100)
  long = int(longsplit[0]) + (int(longsplit[1])/100)

  # Negate longitude since we are west
  long = -long

  # Now get Alt
  for line in contents.split("\n"):
    if "Altitude:" in line:
      string = line.strip()
      break

  string = string[15:90]

  expression = "[0-9][0-9]+"
  p = re.compile(expression)
  results = p.findall(string)
  alt = int(results[0])

  file.close()
  os.remove(filename) # Don't need the file anymore and avoids issues with wget downloading files of same name

  print("Location of target:")
  print("Latitude: " + str(lat))
  print("Longitude: " + str(long))
  print("Altitude: " + str(alt))
  print()
  az, el, range = pm.geodetic2aer(lat, long, alt, WPLat, WPLong, 250) # Stating that West Point has an altitude of 250

  # Round to single decimal point
  az = round(az,1)
  el = round(el,1)

  if el < 0:
    print("Negative elevation.  Setting to 0")
    el = 0

  range = round(range,1)
  print("Azimuth: " + str(az) + "°")
  print("Elevation: " + str(el) + "°")
  print("Range: " + str(range) + " m")
  print()

  #///////
  #Sends serial commands to M2 controller
  azupdate = "APn" + str(az) + "\r;"
  elupdate = "APn" + str(el) + "\r;"

  print("azupdate: " + azupdate)
  print("elupdate: " + elupdate)

  try:
    azser.write(azupdate.encode('utf-8'))
  except:
    print("Failed to write azimuth to rotator")
    #import pdb;pdb.set_trace()
  try:
    elser.write(elupdate.encode('utf-8'))
  except:
      print("Failed to write elevation to rotator")
  #///////

  print()
  print()

  c += 1
  #wait for 2 minutes for Byonics beacon to transmit updated Location
  time.sleep(120)

print("Exceeded max execution time.  Exiting...")